from logging.config import dictConfig
import pathlib
import uvicorn
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError, HTTPException
from starlette import status
from starlette.exceptions import ExceptionMiddleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from app.constants import BASE_URL
from app.exceptions import ValidationException, SMSNotVerifiedException
from app.lib.api_router import APIRouter
from app.services.auth import CustomAuthenticationBackend
from app.services.logging import LogConfig
from app.views import (
    auth_view,
    registration_view,
)

dictConfig(LogConfig().dict())


app = FastAPI(
    docs_url=BASE_URL+"docs",
    openapi_url=BASE_URL+"openapi.json",
)
root_path = pathlib.Path(__file__).parent
root_path.mkdir(parents=True, exist_ok=True)
app.mount(BASE_URL + "media", StaticFiles(directory=str(root_path) + "/media"))

origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(RequestValidationError)
async def request_validation_error_handler(request, exc: RequestValidationError):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder(
            {
                "success": False,
                "error": {
                    "code": "ValidationException",
                    "message": str(exc.errors()),
                },
            }
        ),
    )


@app.exception_handler(HTTPException)
async def http_exception_error_handler1(request, exc: HTTPException):
    if isinstance(exc, ValidationException):
        return JSONResponse(
            status_code=exc.status_code,
            content=jsonable_encoder(
                {
                    "success": False,
                    "error": {
                        "code": exc.detail,
                        "message": exc.message,
                    },
                }
            ),
        )
    if isinstance(exc, SMSNotVerifiedException):
        return JSONResponse(
            status_code=exc.status_code,
            content=jsonable_encoder(
                {
                    "success": False,
                    "error": {
                        "code": exc.detail,
                        "message": "Неверный код",
                    },
                }
            ),
        )
    return JSONResponse(
        status_code=exc.status_code,
        headers=exc.headers,
        content=jsonable_encoder(
            {
                "success": False,
                "error": {
                    "code": exc.detail,
                    "message": "",
                },
            }
        ),
    )


@app.exception_handler(Exception)
async def http_exception_error_handler2(request, exc: Exception):
    return JSONResponse(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        content=jsonable_encoder(
            {
                "success": False,
                "error": {
                    "code": "InternalServerErrorException",
                    "message": "",
                },
            }
        ),
    )


main_router = APIRouter(prefix=BASE_URL)
main_router.include_router(auth_view.router)

registration_router = APIRouter(prefix=BASE_URL+"registration")
registration_router.include_router(registration_view.router)

app.include_router(main_router)
app.include_router(registration_router)


@app.on_event("startup")
async def startup():
    app.add_middleware(AuthenticationMiddleware, backend=CustomAuthenticationBackend())
    app.add_middleware(ExceptionMiddleware, handlers=app.exception_handlers)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000, proxy_headers=True)
