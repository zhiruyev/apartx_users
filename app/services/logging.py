from pydantic import BaseModel


class LogConfig(BaseModel):
    LOG_FORMAT: str = "%(levelprefix)s | %(asctime)s | %(message)s"

    # Logging config
    version = 1
    disable_existing_loggers = False
    formatters = {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    }
    handlers = {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    }
    loggers = {
        "some_backend_logger": {"handlers": ["default"], "level": "INFO"},
    }
