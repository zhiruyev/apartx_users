import base64
import os

from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

from app.constants import (
    ENCODING_TYPE,
    ENCRYPTION_KEY_BITS,
    SENTINEL_BITS,
)
from app.exceptions import (
    DecryptionErrorException,
    EncryptionErrorException,
    IncorrectMessageException,
)


class RSAEncryption:
    def __init__(self):
        self.bits = ENCRYPTION_KEY_BITS

    def _helper(self, key: bytes) -> str:
        return "\\n".join(key.decode(ENCODING_TYPE).splitlines())

    def generate_rsa_keys(self):
        key_pair = RSA.generate(self.bits)
        public_key = self._helper(key_pair.public_key().export_key())
        private_key = self._helper(key_pair.export_key())
        """
        os.environ[AUTH_PUBLIC_KEY] = public_key
        os.environ[AUTH_PRIVATE_KEY] = private_key
        # for local testing :)
        with open(".env", mode="a") as f:
            keys = (
                f"\n{AUTH_PUBLIC_KEY}=\"{public_key}\""
                f"\n{AUTH_PRIVATE_KEY}=\"{private_key}\"\n"
            )
            f.write(keys)
        """
        return public_key, private_key

    @staticmethod
    def _cipher(key: bytes):
        key = RSA.import_key(key)
        return PKCS1_v1_5.new(key)

    def encrypt(self, public_key: bytes, message: str) -> str:
        encryptor = self._cipher(public_key)
        try:
            encrypted = base64.b64encode(
                encryptor.encrypt(message.encode(ENCODING_TYPE))
            )
            encrypted = encrypted.decode(ENCODING_TYPE)
        except Exception:
            raise EncryptionErrorException()
        else:
            return encrypted

    def decrypt(self, private_key: bytes, message: str) -> str:
        decryptor = self._cipher(private_key)
        sentinel = get_random_bytes(SENTINEL_BITS)
        try:
            decrypted = decryptor.decrypt(
                ciphertext=base64.b64decode(message.encode(ENCODING_TYPE)),
                sentinel=sentinel,
            )
            if decrypted == sentinel:
                raise IncorrectMessageException()
            decrypted = decrypted.decode(ENCODING_TYPE)
        except Exception:
            raise DecryptionErrorException()
        else:
            return decrypted
