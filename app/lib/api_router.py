import logging
import time
import uuid
from typing import Any, Callable

from fastapi import APIRouter as FastAPIRouter
from fastapi.routing import APIRoute
from fastapi.types import DecoratedCallable
from starlette.requests import Request
from starlette.responses import Response

from app.context_vars import request_uuid_context


class APIRouter(FastAPIRouter):
    def api_route(
        self, path: str, *, include_in_schema: bool = True, **kwargs: Any
    ) -> Callable[[DecoratedCallable], DecoratedCallable]:
        if path.endswith("/"):
            path = path[:-1]

        add_path = super().api_route(
            path, include_in_schema=include_in_schema, **kwargs
        )

        alternate_path = path + "/"
        add_alternate_path = super().api_route(
            alternate_path, include_in_schema=False, **kwargs
        )

        def decorator(func: DecoratedCallable) -> DecoratedCallable:
            add_alternate_path(func)
            return add_path(func)

        return decorator


class BaseRoute(APIRoute):
    logger = logging.getLogger("some_backend_logger")

    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            request_data = dict(request)
            request_uuid = str(uuid.uuid4())
            request_uuid_context.set(request_uuid)
            self.logger.info(
                {
                    "request_uuid": request_uuid,
                    "method": request_data.get("method"),
                    "path": request_data.get("path"),
                    "user": request_data.get("user"),
                    "query_params": dict(request.query_params),
                }
            )
            before = time.time()
            response: Response = await original_route_handler(request)
            duration = time.time() - before
            response.headers["X-Response-Time"] = str(duration)
            response_data = response.__dict__
            self.logger.info(
                {
                    "request_uuid": request_uuid,
                    "status_code": response_data.get("status_code"),
                    "body": response_data.get("body", b"").decode("utf-8"),
                }
            )

            return response

        return custom_route_handler
