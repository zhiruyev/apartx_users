from pydantic import BaseModel


class UserEntity(BaseModel):
    customer_external_id: str
