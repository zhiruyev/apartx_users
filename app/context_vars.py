from contextvars import ContextVar

request_uuid_context: ContextVar[str] = ContextVar("request_uuid_context")
