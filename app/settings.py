from envparse import Env

env = Env()
env.read_envfile()

APP_ENV = env.str("APP_ENV")
DB_USERNAME = env.str("DB_USERNAME")
DB_PASSWORD = env.str("DB_PASSWORD")
DB_NAME = env.str("DB_NAME")
DB_HOST = env.str("DB_HOST")
DB_PORT = env.int("DB_PORT")

JWT_LIFE_TIME = env.int("JWT_LIFE_TIME")
JWT_REFRESH_LIFE_TIME = env.int("JWT_REFRESH_LIFE_TIME")
JWT_PRIVATE_KEY = env.str("JWT_PRIVATE_KEY", default="").replace(r"\n", "\n")
JWT_PUBLIC_KEY = env.str("JWT_PUBLIC_KEY", default="").replace(r"\n", "\n")

STATIC_FOLDER_NAME = "media"
APP_CLOSED = bool(int(env.str("APP_CLOSED", default="0")))
APP_LOGIN_LIST = env.str("APP_LOGIN_LIST", default="")

REG_BASIC_USER = env.str("REG_BASIC_USER")
REG_BASIC_PASSWORD = env.str("REG_BASIC_PASSWORD")
