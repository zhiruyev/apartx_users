from typing import Optional, Union
from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession
from uuid import uuid4

from app.models import UserModel
from app.views.entities.input.create_user_input_entity import (
    CreateUserInputEntity,
)

from app.services.enctyption import RSAEncryption


class RegistrationHandler:
    def __init__(self, session: AsyncSession):
        self.session = session
        self.encryption = RSAEncryption()
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    async def check_for_exist(
        self,
        input_entity: CreateUserInputEntity,
    ) -> Optional[UserModel]:
        user_by_login = await UserModel.get_by_login(session=self.session, login=input_entity.login)
        return user_by_login

    async def check_for_exist_by_iin(self, iin: str) -> bool:
        user_by_iin = await UserModel.get_by_iin(session=self.session, iin=iin)
        return bool(user_by_iin)

    async def create_user(self, input_entity: CreateUserInputEntity):
        user = UserModel(
            login=input_entity.login,
            email=input_entity.email,
            password=self.pwd_context.hash(input_entity.password),
            customer_external_id=str(uuid4()),
        )
        await user.create(session=self.session)
        await self.session.commit()
