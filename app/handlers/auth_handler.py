from typing import Optional

from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.requests import Request

from app.constants import KeyTypeEnum
from app.models import UserModel
from app.models.key_model import KeyModel
from app.services.enctyption import RSAEncryption
from app.views.entities.response.public_key_entity import PublicKeyResponseEntity


class AuthHandler:
    def __init__(self, session: AsyncSession, user: Optional[UserModel], request: Request = None):
        self.session = session
        self.user = user
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        self.encryption = RSAEncryption()
        self.request = request

    async def get_public_key(self) -> PublicKeyResponseEntity:
        public_key = await KeyModel.get_public_key(KeyTypeEnum.MOBILE.value, self.session)
        public_key = public_key.replace(r"\n", "\n")
        return PublicKeyResponseEntity(public_key=public_key)
