from pydantic import BaseModel


class CreateUserInputEntity(BaseModel):
    email: str
    login: str
    password: str

