from pydantic import BaseModel


class PublicKeyResponseEntity(BaseModel):
    public_key: str

    class Config:
        orm_mode = True
