from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.constants import KeyTypeEnum
from app.dependencies import (
    get_session,
    reg_basic_auth,
)
from app.exceptions import UserLoginAlreadyExistException
from app.handlers.registration_handler import RegistrationHandler
from app.lib.api_router import (
    APIRouter,
    BaseRoute,
)
from app.lib.rsa_key_utils import rsa_key_replace_n
from app.models.key_model import KeyModel
from app.services.enctyption import RSAEncryption
from app.views.entities.input.create_user_input_entity import CreateUserInputEntity

router = APIRouter(route_class=BaseRoute)


@router.post(
    "/create_user_encrypted/",
    tags=["registration"],
    response_model=None,
    response_model_by_alias=True,
)
async def create_user_encrypted(
    input_entity: CreateUserInputEntity,
    session: AsyncSession = Depends(get_session),
    permission=Depends(reg_basic_auth),
):
    private_key = await KeyModel.get_private_key(type=KeyTypeEnum.MOBILE.value, session=session)
    private_key = rsa_key_replace_n(private_key)
    decrypted_login = RSAEncryption().decrypt(private_key, input_entity.login)
    decrypted_password = RSAEncryption().decrypt(private_key, input_entity.password)
    input_entity.login = decrypted_login
    input_entity.password = decrypted_password
    user_by_iin, user_by_login = await RegistrationHandler(session=session).check_for_exist(input_entity=input_entity)
    if user_by_login:
        raise UserLoginAlreadyExistException()
    await RegistrationHandler(session=session).create_user(input_entity=input_entity)
