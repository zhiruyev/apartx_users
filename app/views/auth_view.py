from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.dependencies import get_session
from app.handlers.auth_handler import AuthHandler
from app.lib.api_router import (
    APIRouter,
    BaseRoute,
)
from app.views.entities.response.public_key_entity import PublicKeyResponseEntity

router = APIRouter(route_class=BaseRoute)


@router.get(
    "/auth/public_key",
    tags=["Auth"],
    response_model=PublicKeyResponseEntity,
    response_model_by_alias=False,
)
async def public_key(
    session: AsyncSession = Depends(get_session),
):
    response_entity = await AuthHandler(session, None).get_public_key()
    return response_entity
