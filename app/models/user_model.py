import datetime

from sqlalchemy.ext.asyncio import AsyncSession

from app.models import Base

from sqlalchemy import (
    BigInteger,
    Column,
    String,
    DateTime,
)
from sqlalchemy.sql.expression import text


class UserModel(Base):
    __tablename__ = "users"

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    email = Column(String, nullable=False, index=True)
    login = Column(String, nullable=False, index=True)
    password = Column(String, nullable=False)
    customer_external_id = Column(String, nullable=False, index=True)

    created_at = Column(
        DateTime,
        default=datetime.datetime.utcnow,
        server_default=text("timezone('utc', now())"),
        nullable=False,
    )

    async def create(self, session: AsyncSession):
        session.add(self)
        await session.flush()

    async def update(self, session: AsyncSession):
        await session.merge(self)
        await session.flush()
