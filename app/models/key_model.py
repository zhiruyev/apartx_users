import datetime
from sqlalchemy import (
    Column,
    String,
    BigInteger,
    DateTime,
    text,
    select,
)
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy_utils import ChoiceType

from app.constants import KeyTypeEnum
from app.models import Base
from app.services.enctyption import RSAEncryption


class KeyModel(Base):
    __tablename__ = "keys"

    id = Column(BigInteger, primary_key=True)
    type = Column(ChoiceType(KeyTypeEnum, impl=String()))
    public_key = Column(String, nullable=False)
    private_key = Column(String, nullable=False)
    created_at = Column(
        DateTime,
        default=datetime.datetime.utcnow,
        server_default=text("timezone('utc', now())"),
        nullable=False,
    )
    updated_at = Column(
        DateTime,
        default=datetime.datetime.utcnow,
        server_default=text("timezone('utc', now())"),
        onupdate=datetime.datetime.utcnow,
        nullable=False,
    )

    async def create(self, session: AsyncSession):
        session.add(self)
        await session.commit()

    async def update(self, session: AsyncSession):
        await session.merge(self)
        await session.flush()

    @classmethod
    async def get_public_key(cls, type: str, session: AsyncSession) -> str:
        query = await session.execute(select(cls).filter(cls.type == type))
        if obj := query.scalars().first():
            return obj.public_key
        public_key, private_key = RSAEncryption().generate_rsa_keys()
        key = KeyModel(
            type=type,
            public_key=public_key,
            private_key=private_key,
        )
        await key.create(session=session)
        return key.public_key

    @classmethod
    async def get_private_key(cls, type: str, session: AsyncSession) -> str:
        query = await session.execute(select(cls).filter(cls.type == type))
        obj = query.scalars().one()
        return obj.private_key
