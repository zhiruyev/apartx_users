from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


from .. import settings as stgs

Base = declarative_base()
engine = create_async_engine(
    f"postgresql+asyncpg://{stgs.DB_USERNAME}:{stgs.DB_PASSWORD}"
    f"@{stgs.DB_HOST}:{stgs.DB_PORT}/{stgs.DB_NAME}",
    future=True,
    echo=False,
    pool_size=5,
    pool_timeout=30,
)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)

from .user_model import UserModel
