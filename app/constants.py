from enum import Enum

BASE_URL = "/apartx_users"

ENCRYPTION_KEY_BITS = 2048
SENTINEL_BITS = 16
ENCODING_TYPE = "utf-8"


class KeyTypeEnum(Enum):
    WEB = "WEB"
    MOBILE = "MOBILE"
