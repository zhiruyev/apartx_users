import secrets

from fastapi import Depends
from fastapi.security import HTTPBasicCredentials, HTTPBasic
from sqlalchemy.ext.asyncio import AsyncSession

from app.exceptions import NotAuthorizedException
from app.models import async_session
from app import settings as stgs


async def get_session() -> AsyncSession:
    async with async_session() as session:
        yield session

reg_security = HTTPBasic()


async def reg_basic_auth(credentials: HTTPBasicCredentials = Depends(reg_security)):
    if not all([secrets.compare_digest(credentials.username, stgs.REG_BASIC_USER),
                secrets.compare_digest(credentials.password, stgs.REG_BASIC_PASSWORD)]):
        raise NotAuthorizedException()
    return credentials.username, credentials.password
